package org.mte.numecoeval.calculs.factory;

import org.instancio.Instancio;
import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.DataCenterDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.EtapeDTO;
import org.mte.numecoeval.topic.data.HypotheseDTO;
import org.mte.numecoeval.topic.data.ImpactEquipementDTO;
import org.mte.numecoeval.topic.data.ImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.ImpactReseauDTO;
import org.mte.numecoeval.topic.data.IndicateurImpactEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.IndicateurImpactEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageCalculEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageCalculIndicateurApplicationDTO;
import org.mte.numecoeval.topic.data.MessageCalculMessagerieDTO;
import org.mte.numecoeval.topic.data.MixElectriqueDTO;
import org.mte.numecoeval.topic.data.TypeEquipementDTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestDataFactory {

    public static List<EtapeDTO> etapesACV() {
        return Arrays.asList(
                EtapeDTO.builder().code("FABRICATION").libelle("Fabrication").build(),
                EtapeDTO.builder().code("UTILISATION").libelle("Utilisation").build(),
                EtapeDTO.builder().code("FIN_DE_VIE").libelle("Fin de vie").build(),
                EtapeDTO.builder().code("DISTRIBUTION").libelle("Distribution").build()
        );
    }

    public static List<CritereDTO> criteres() {
        return Arrays.asList(
                CritereDTO.builder().nomCritere("Changement climatique").unite("kg CO_{2} eq").build(),
                CritereDTO.builder().nomCritere("Émissions de particules fines").unite("Diseaseincidence").build(),
                CritereDTO.builder().nomCritere("Radiations ionisantes").unite("kBq U-235 eq").build(),
                CritereDTO.builder().nomCritere("Acidification").unite("mol H^{+} eq").build(),
                CritereDTO.builder().nomCritere("Épuisement des ressources naturelles (minérales et métaux)").unite("kg Sb eq").build()
        );
    }

    public static HypotheseDTO hypotheseDTO(String code, String valeur){
        return HypotheseDTO.builder()
                .valeur(valeur)
                .code(code)
                .build();
    }

    public static TypeEquipementDTO typeEquipement(String type, String refParDefaut, Double dureeDeVieParDefaut) {
        return TypeEquipementDTO.builder()
                .type(type)
                .refEquipementParDefaut(refParDefaut)
                .dureeVieDefaut(dureeDeVieParDefaut)
                .build();
    }

    public static CorrespondanceRefEquipementDTO correspondanceRefEquipement(String modeleEquipementSource, String refCible) {
        return CorrespondanceRefEquipementDTO.builder()
                .modeleEquipementSource(modeleEquipementSource)
                .refEquipementCible(refCible)
                .build();
    }

    public static DataCenterDTO dataCenter(String nomCourtDataCenter, String nomLongDataCenter, String localisation, Double pue) {
        return DataCenterDTO.builder()
                .nomCourtDatacenter(nomCourtDataCenter)
                .nomLongDatacenter(nomLongDataCenter)
                .localisation(localisation)
                .pue(pue)
                .build();
    }

    public static EquipementPhysiqueDTO equipementPhysique(DataCenterDTO dataCenter, String equipement, String typeEquipement, String paysUtilisation, LocalDate dateAchat, Double quantite, Float goTelecharge, String modele, boolean isServeur) {
        return EquipementPhysiqueDTO.builder()
                .nomEquipementPhysique(equipement)
                .type(typeEquipement)
                .paysDUtilisation(paysUtilisation)
                .dateAchat(dateAchat)
                .quantite(quantite)
                .goTelecharge(goTelecharge)
                .modele(modele)
                .nomCourtDatacenter(dataCenter != null ? dataCenter.getNomCourtDatacenter() : null)
                .serveur(isServeur)
                .build();
    }

    public static MixElectriqueDTO mixElectrique(String critere, String pays, Double valeur ){
        return MixElectriqueDTO.builder()
                .critere(critere)
                .pays(pays)
                .valeur(valeur)
                .build();
    }

    public static ImpactEquipementDTO impactEquipement(String type, String refEquipement, Double valeur, Double consoElecMoyenne){
        return ImpactEquipementDTO.builder()
                .type(type)
                .refEquipement(refEquipement)
                .valeur(valeur)
                .consoElecMoyenne(consoElecMoyenne)
                .build();
    }

    public static ImpactReseauDTO impactReseau(String etape, String critere, String refReseau, Double valeur){
        return ImpactReseauDTO.builder()
                .refReseau(refReseau)
                .critere(critere)
                .etapeACV(etape)
                .valeur(valeur)
                .build();
    }

    public static ImpactMessagerieDTO impactsMessagerie(String critere, Double coefficientDirecteur, Double constanteOrdonneeOrigine ){
        return ImpactMessagerieDTO.builder()
                .critere(critere)
                .constanteCoefficientDirecteur(coefficientDirecteur)
                .constanteOrdonneeOrigine(constanteOrdonneeOrigine)
                .build();
    }

    public static List<ImpactEquipementDTO> defaultImpactsEquipement(String type, String refEquipement, Double valeur, Double consoElecMoyenne){
        List<ImpactEquipementDTO> impacts = new ArrayList<>();

        criteres().forEach(critereDTO ->
                etapesACV().forEach(etapeDTO -> {
                    impacts.add(impactEquipement(type, refEquipement, valeur, consoElecMoyenne));
                }));

        return impacts;
    }

    public static List<ImpactReseauDTO> defaultImpactsResau(String refReseau, Double valeur){
        List<ImpactReseauDTO> impacts = new ArrayList<>();

        criteres().forEach(critereDTO ->
                etapesACV().forEach(etapeDTO -> {
                    impacts.add(impactReseau(etapeDTO.getCode(), critereDTO.getNomCritere(), refReseau, valeur));
                }));

        return impacts;
    }

    public static List<MixElectriqueDTO> defaultMixElectriques(String pays, Double valeur){
        List<MixElectriqueDTO> dtos = new ArrayList<>();

        criteres().forEach(critereDTO -> dtos.add(mixElectrique(critereDTO.getNomCritere(), pays, valeur)));

        return dtos;
    }

    private static List<ImpactMessagerieDTO> defaultImpactsMessagerie(double coefficientDirecteur, double constanteOrdonneeOrigine) {
        List<ImpactMessagerieDTO> dtos = new ArrayList<>();

        criteres().forEach(critereDTO -> dtos.add(impactsMessagerie(critereDTO.getNomCritere(), coefficientDirecteur, constanteOrdonneeOrigine)));

        return dtos;
    }

    private static IndicateurImpactEquipementPhysiqueDTO indicateurImpactEquipementPhysique(String nomEquipementPhysique, String nomCritere, String etape, Double valeur, Double consoElecMoyenne) {
        return IndicateurImpactEquipementPhysiqueDTO.builder()
                .nomEquipement(nomEquipementPhysique)
                .critere(nomCritere)
                .etapeACV(etape)
                .impactUnitaire(valeur)
                .consoElecMoyenne(consoElecMoyenne)
                .build();
    }

    public static List<IndicateurImpactEquipementPhysiqueDTO> defaultIndicateurImpactEquipementPhysique(String nomEquipementPhysique, Double valeur, Double consoElecMoyenne){
        List<IndicateurImpactEquipementPhysiqueDTO> impacts = new ArrayList<>();

        criteres().forEach(critereDTO ->
                etapesACV().forEach(etapeDTO -> {
                    impacts.add(indicateurImpactEquipementPhysique(nomEquipementPhysique, critereDTO.getNomCritere(), etapeDTO.getCode(), valeur, consoElecMoyenne));
                }));

        return impacts;
    }

    private static IndicateurImpactEquipementVirtuelDTO indicateurImpactEquipementVirtuel(String nomVM, String nomEquipementPhysique, String nomCritere, String etape, Double valeur, Double consoElecMoyenne) {
        return IndicateurImpactEquipementVirtuelDTO.builder()
                .nomEquipementVirtuel(nomVM)
                .nomEquipement(nomEquipementPhysique)
                .critere(nomCritere)
                .etapeACV(etape)
                .impactUnitaire(valeur)
                .consoElecMoyenne(consoElecMoyenne)
                .build();
    }

    public static List<IndicateurImpactEquipementVirtuelDTO> defaultIndicateurImpactEquipementVirtuel(String nomVM, String nomEquipementPhysique, Double valeur, Double consoElecMoyenne){
        List<IndicateurImpactEquipementVirtuelDTO> impacts = new ArrayList<>();

        criteres().forEach(critereDTO ->
                etapesACV().forEach(etapeDTO -> {
                    impacts.add(indicateurImpactEquipementVirtuel(nomVM, nomEquipementPhysique, critereDTO.getNomCritere(), etapeDTO.getCode(), valeur, consoElecMoyenne));
                }));

        return impacts;
    }

    public static CalculEquipementPhysiqueDTO getValidInputCalculEquipementPhysiqueDTO() {
        var calcul = Instancio.of(CalculEquipementPhysiqueDTO.class).create();
        var refParDefaut = "refParDefaut";
        var refCible = "refParDefaut";
        var type = "Laptop";
        calcul.getEquipementPhysique().setType(type);
        calcul.setTypeEquipement(typeEquipement(type, refParDefaut, 5.0));
        calcul.setCorrespondanceRefEquipement(
                correspondanceRefEquipement(calcul.getEquipementPhysique().getModele(),refCible)
        );
        var pays = "France";
        var paysDataCenter = "Monaco";
        calcul.getEquipementPhysique().setPaysDUtilisation(pays);
        calcul.getEquipementPhysique().getDataCenter().setLocalisation(paysDataCenter);

        calcul.setEtapes(etapesACV());
        calcul.setCriteres(criteres());
        var impacts = defaultImpactsEquipement(calcul.getEquipementPhysique().getType(),refCible, 10.0, 5.0);
        impacts.addAll(defaultImpactsEquipement(calcul.getEquipementPhysique().getType(),refParDefaut, 10.0, 5.0));
        calcul.setImpactsEquipement(impacts);
        calcul.setImpactsReseau(defaultImpactsResau("impactReseauMobileMoyen", 10.0));

        var mixElec = defaultMixElectriques(pays, 100.0);
        mixElec.addAll(defaultMixElectriques(paysDataCenter, 125.0));
        calcul.setMixElectriques(mixElec);

        calcul.setHypotheses(new ArrayList<>());
        calcul.getHypotheses().add(hypotheseDTO("dureeVieParDefaut", "5.0"));
        calcul.getHypotheses().add(hypotheseDTO("PUEParDefaut", "1.0"));

        return calcul;
    }

    public static MessageCalculEquipementVirtuelDTO getValidInputMessageCalculEquipementVirtuelDTO() {
        var calcul = Instancio.of(MessageCalculEquipementVirtuelDTO.class).create();
        calcul.setImpactsEquipement(defaultIndicateurImpactEquipementPhysique(calcul.getEquipementVirtuel().getNomEquipementPhysique(), 100.0, 5.0));
        calcul.setNbEquipementsVirtuels(1);
        calcul.setNbTotalVCPU(1);

        return calcul;
    }

    public static MessageCalculIndicateurApplicationDTO getValidInputMessageCalculIndicateurApplicationDTO() {
        var calcul = Instancio.of(MessageCalculIndicateurApplicationDTO.class).create();
        calcul.setIndicateursEquipementsVirtuels(defaultIndicateurImpactEquipementVirtuel(calcul.getApplication().getNomEquipementVirtuel(), calcul.getApplication().getNomEquipementPhysique(), 100.0, 5.0));
        calcul.setNbApplications(1);

        return calcul;
    }

    public static MessageCalculMessagerieDTO getValidInputMessageCalculMessagerieDTO() {
        var calcul = Instancio.of(MessageCalculMessagerieDTO.class).create();
        calcul.setCriteres(criteres());
        calcul.setImpactsMessagerie(defaultImpactsMessagerie(100.0, 5.0));

        return calcul;
    }
}
