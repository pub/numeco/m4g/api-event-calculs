package org.mte.numecoeval.calculs.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.DureeDeVieEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.factory.TestDataFactory;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.service.CalculEquipementPhysiqueIntegrationService;

import static org.junit.jupiter.api.Assertions.*;

class CalculEquipementPhysiqueIntegrationServiceTest {

    CalculEquipementPhysiqueIntegrationService service;

    @BeforeEach
    void setup() {
        service = new CalculEquipementPhysiqueIntegrationService(
                new EntreesMapperImpl(),
                new ReferentielMapperImpl(),
                new IndicateursMapperImpl(),
                new CalculImpactEquipementPhysiqueServiceImpl(
                        new DureeDeVieEquipementPhysiqueServiceImpl(),
                        new ObjectMapper()
                )
        );
    }
    
    @Test
    void whenValidInput_shouldReturnMessageIndicateurOK() {
        // Given
        var calcul = TestDataFactory.getValidInputCalculEquipementPhysiqueDTO();

        // When
        var result = service.calculImpactEquipementPhysique(calcul);

        // Then
        assertNotNull(result);
        assertFalse(result.getImpactsEquipement().isEmpty());
        assertEquals( calcul.getEquipementPhysique(), result.getEquipementPhysique() );
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), result.getImpactsEquipement().size());
    }

    @Test
    void whenInvalidInput_shouldReturnMessageWithEmptyList() {
        // Given
        var calcul = TestDataFactory.getValidInputCalculEquipementPhysiqueDTO();
        calcul.setCriteres(null);
        calcul.setEtapes(null);

        // When
        var result = service.calculImpactEquipementPhysique(calcul);

        // Then
        assertNotNull(result);
        assertTrue(result.getImpactsEquipement().isEmpty());
        assertEquals( calcul.getEquipementPhysique(), result.getEquipementPhysique() );
        assertEquals(0, result.getImpactsEquipement().size());
    }

}
