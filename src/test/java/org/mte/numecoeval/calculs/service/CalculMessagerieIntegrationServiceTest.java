package org.mte.numecoeval.calculs.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactMessagerieServiceImpl;
import org.mte.numecoeval.calculs.factory.TestDataFactory;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.service.CalculMessagerieIntegrationService;

import static org.junit.jupiter.api.Assertions.*;

class CalculMessagerieIntegrationServiceTest {

    CalculMessagerieIntegrationService service;

    @BeforeEach
    void setup() {
        service = new CalculMessagerieIntegrationService(
                new EntreesMapperImpl(),
                new ReferentielMapperImpl(),
                new IndicateursMapperImpl(),
                new CalculImpactMessagerieServiceImpl(
                        new ObjectMapper()
                )
        );
    }

    @Test
    void whenValidInput_shouldReturnMessageIndicateurOK() {
        // Given
        var calcul = TestDataFactory.getValidInputMessageCalculMessagerieDTO();

        // When
        var result = service.calculImpactMessagerie(calcul);

        // Then
        assertNotNull(result);
        assertEquals(calcul.getMessagerie(), result.getMessagerie());
        assertFalse(result.getIndicateurs().isEmpty());
    }

    @Test
    void whenInvalidInput_shouldReturnMessageWithEmptyList() {
        // Given
        var calcul = TestDataFactory.getValidInputMessageCalculMessagerieDTO();
        calcul.setCriteres(null);

        // When
        var result = service.calculImpactMessagerie(calcul);

        // Then
        assertNotNull(result);
        assertTrue(result.getIndicateurs().isEmpty());
    }


}
