package org.mte.numecoeval.calculs.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;
import org.mte.numecoeval.calculs.factory.TestDataFactory;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.service.CalculEquipementVirtuelIntegrationService;

import static org.junit.jupiter.api.Assertions.*;

class CalculEquipementVirtuelIntegrationServiceTest {

    CalculEquipementVirtuelIntegrationService service;

    @BeforeEach
    void setup() {
        service = new CalculEquipementVirtuelIntegrationService(
                new EntreesMapperImpl(),
                new IndicateursMapperImpl(),
                new CalculImpactEquipementVirtuelServiceImpl(
                        new ObjectMapper()
                )
        );
    }

    @Test
    void whenValidInput_shouldReturnMessageIndicateurOK() {
        // Given
        var calcul = TestDataFactory.getValidInputMessageCalculEquipementVirtuelDTO();

        // When
        var result = service.calculImpactEquipementVirtuel(calcul);

        // Then
        assertNotNull(result);
        assertEquals(calcul.getEquipementVirtuel(), result.getEquipementVirtuel());
        assertFalse(result.getImpactsEquipement().isEmpty());
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), result.getImpactsEquipement().size());
    }

    @Test
    void whenInvalidInput_shouldReturnMessageWithEmptyList() {
        // Given
        var calcul = TestDataFactory.getValidInputMessageCalculEquipementVirtuelDTO();
        calcul.setImpactsEquipement(null);

        // When
        var result = service.calculImpactEquipementVirtuel(calcul);

        // Then
        assertNotNull(result);
        assertTrue(result.getImpactsEquipement().isEmpty());
    }
}
