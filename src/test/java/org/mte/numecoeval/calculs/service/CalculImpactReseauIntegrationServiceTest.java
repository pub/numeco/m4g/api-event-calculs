package org.mte.numecoeval.calculs.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactReseauServiceImpl;
import org.mte.numecoeval.calculs.factory.TestDataFactory;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.service.CalculImpactReseauIntegrationService;

import static org.junit.jupiter.api.Assertions.*;

class CalculImpactReseauIntegrationServiceTest {

    CalculImpactReseauIntegrationService service;

    @BeforeEach
    void setup() {
        service = new CalculImpactReseauIntegrationService(
                new EntreesMapperImpl(),
                new ReferentielMapperImpl(),
                new IndicateursMapperImpl(),
                new CalculImpactReseauServiceImpl(
                        new ObjectMapper()
                )
        );
    }
    
    @Test
    void whenValidInput_shouldReturnMessageIndicateurOK() {
        // Given
        var calcul = TestDataFactory.getValidInputCalculEquipementPhysiqueDTO();

        // When
        var result = service.calculImpactReseau(calcul);

        // Then
        assertNotNull(result);
        assertFalse(result.getImpactsReseaux().isEmpty());
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), result.getImpactsReseaux().size());
    }

    @Test
    void whenInvalidInput_shouldReturnMessageWithEmptyList() {
        // Given
        var calcul = TestDataFactory.getValidInputCalculEquipementPhysiqueDTO();
        calcul.setCriteres(null);
        calcul.setEtapes(null);

        // When
        var result = service.calculImpactReseau(calcul);

        // Then
        assertNotNull(result);
        assertTrue(result.getImpactsReseaux().isEmpty());
    }

}
