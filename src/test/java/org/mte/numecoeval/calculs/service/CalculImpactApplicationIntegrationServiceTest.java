package org.mte.numecoeval.calculs.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactApplicationServiceImpl;
import org.mte.numecoeval.calculs.factory.TestDataFactory;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapperImpl;
import org.mte.numecoeval.calculs.infrastructure.service.CalculImpactApplicationIntegrationService;

import static org.junit.jupiter.api.Assertions.*;

class CalculImpactApplicationIntegrationServiceTest {

    CalculImpactApplicationIntegrationService service;

    @BeforeEach
    void setup() {
        service = new CalculImpactApplicationIntegrationService(
                new EntreesMapperImpl(),
                new IndicateursMapperImpl(),
                new CalculImpactApplicationServiceImpl(
                        new ObjectMapper()
                )
        );
    }

    @Test
    void whenValidInput_shouldReturnMessageIndicateurOK() {
        // Given
        var calcul = TestDataFactory.getValidInputMessageCalculIndicateurApplicationDTO();

        // When
        var result = service.calculImpactApplication(calcul);

        // Then
        assertNotNull(result);
        assertEquals(calcul.getApplication(), result.getApplication());
        assertFalse(result.getIndicateursApplication().isEmpty());
    }

    @Test
    void whenInvalidInput_shouldReturnMessageWithEmptyList() {
        // Given
        var calcul = TestDataFactory.getValidInputMessageCalculIndicateurApplicationDTO();
        calcul.setIndicateursEquipementsVirtuels(null);

        // When
        var result = service.calculImpactApplication(calcul);

        // Then
        assertNotNull(result);
        assertTrue(result.getIndicateursApplication().isEmpty());
    }


}
