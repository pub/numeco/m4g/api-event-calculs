package org.mte.numecoeval.calculs.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.concurrent.TimeUnit;

@Slf4j
@EnableCaching
public class CacheConfig {

    @CacheEvict(value = {
            "Etapes",
            "Criteres",
            "Hypothese",
            "TypeEquipement",
            "CorrespondanceRefEquipement",
            "ImpactEquipement",
            "ImpactReseau",
            "MixElectrique",
            "ImpactMessagerie",
    }, allEntries = true)
    @Scheduled(fixedRateString = "${numecoeval.cache.ttl}", timeUnit = TimeUnit.MINUTES)
    public void emptyAllCaches() {
        log.info("Emptying all caches");
    }
}
