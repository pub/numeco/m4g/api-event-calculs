package org.mte.numecoeval.calculs.infrastructure.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactApplication;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicationService;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapper;
import org.mte.numecoeval.topic.data.IndicateurImpactEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageCalculIndicateurApplicationDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurApplicationDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Slf4j
@ConditionalOnProperty(
        value = "numecoeval.features.app"
)
@Service
@AllArgsConstructor
public class CalculImpactApplicationIntegrationService {

    EntreesMapper entreesMapper;

    IndicateursMapper indicateursMapper;

    CalculImpactApplicationService calculImpactApplicationService;

    @ServiceActivator(
            inputChannel = "calculImpactApplication",
            outputChannel = "outputCalculImpactApplicationToKafka"
    )
    public MessageIndicateurApplicationDTO calculImpactApplication(MessageCalculIndicateurApplicationDTO calculIndicateurApplicationDTO) {
        log.info("Calcul Impact Application : Nom Application : {}, Type Environnement: {}, : Nom Equipement Virtuel : {}, Nom Equipement Physique : {}",
                calculIndicateurApplicationDTO.getApplication().getNomApplication(),
                calculIndicateurApplicationDTO.getApplication().getTypeEnvironnement(),
                calculIndicateurApplicationDTO.getApplication().getNomEquipementVirtuel(),
                calculIndicateurApplicationDTO.getApplication().getNomEquipementPhysique()
        );
        LocalDateTime dateCalcul = LocalDateTime.now();
        MessageIndicateurApplicationDTO messageIndicateurDTO = new MessageIndicateurApplicationDTO();
        messageIndicateurDTO.setApplication(calculIndicateurApplicationDTO.getApplication());
        messageIndicateurDTO.setIndicateursApplication(new ArrayList<>());
        for(IndicateurImpactEquipementVirtuelDTO impactDTO : CollectionUtils.emptyIfNull(calculIndicateurApplicationDTO.getIndicateursEquipementsVirtuels())) {
            var indicateur = calculImpactApplicationService.calculImpactApplicatif(
                    DemandeCalculImpactApplication.builder()
                            .dateCalcul(dateCalcul)
                            .application(entreesMapper.toDomain(calculIndicateurApplicationDTO.getApplication()))
                            .nbApplications(calculIndicateurApplicationDTO.getNbApplications())
                            .impactEquipementVirtuel(indicateursMapper.toDomain(impactDTO))
                            .build()
            );
            messageIndicateurDTO.addIndicateursApplicationItem(indicateursMapper.toDTO(indicateur));
        }

        return messageIndicateurDTO;
    }
}
