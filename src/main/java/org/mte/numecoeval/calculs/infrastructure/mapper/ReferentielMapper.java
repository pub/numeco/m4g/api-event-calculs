package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCorrespondanceRefEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielCritere;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielEtapeACV;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielHypothese;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactEquipement;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielImpactReseau;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielMixElectrique;
import org.mte.numecoeval.calculs.domain.data.referentiel.ReferentielTypeEquipement;
import org.mte.numecoeval.topic.data.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.EtapeDTO;
import org.mte.numecoeval.topic.data.HypotheseDTO;
import org.mte.numecoeval.topic.data.ImpactEquipementDTO;
import org.mte.numecoeval.topic.data.ImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.ImpactReseauDTO;
import org.mte.numecoeval.topic.data.MixElectriqueDTO;
import org.mte.numecoeval.topic.data.TypeEquipementDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReferentielMapper {

    ReferentielEtapeACV toEtape(EtapeDTO etapeDTO);

    List<ReferentielEtapeACV> toListEtape(List<EtapeDTO> etapeDTO);

    ReferentielCritere toCritere(CritereDTO critereDTO);

    List<ReferentielCritere> toListCritere(List<CritereDTO> critereDTO);

    @Mapping(source = "valeur", target = "impactReseauMobileMoyen")
    ReferentielImpactReseau toImpactReseau(ImpactReseauDTO impactReseauDTO);

    List<ReferentielImpactReseau> toListImpactReseau(List<ImpactReseauDTO> impactReseauDTO);

    ReferentielHypothese toHypothese(HypotheseDTO hypotheseDTO);

    List<ReferentielHypothese> toListHypothese(List<HypotheseDTO> hypotheseDTO);

    ReferentielImpactEquipement toImpactEquipement(ImpactEquipementDTO impactEquipementDTO);

    List<ReferentielImpactEquipement> toListImpactEquipement(List<ImpactEquipementDTO> impactEquipementDTO);

    ReferentielMixElectrique toMixElectrique(MixElectriqueDTO mixElectriqueDTO);

    List<ReferentielMixElectrique> toListMixElectrique(List<MixElectriqueDTO> mixElectriqueDTO);

    ReferentielImpactMessagerie toImpactMessagerie(ImpactMessagerieDTO impactMessagerieDTO);

    List<ReferentielImpactMessagerie> toListImpactMessagerie(List<ImpactMessagerieDTO> impactMessagerieDTO);

    ReferentielCorrespondanceRefEquipement toCorrespondanceRefEquipement(CorrespondanceRefEquipementDTO dto);

    ReferentielTypeEquipement toTypeEquipement(TypeEquipementDTO dto);
}
