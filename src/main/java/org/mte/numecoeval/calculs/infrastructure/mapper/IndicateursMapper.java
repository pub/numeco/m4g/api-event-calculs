package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactApplication;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactReseau;
import org.mte.numecoeval.topic.data.IndicateurImpactApplicationDTO;
import org.mte.numecoeval.topic.data.IndicateurImpactEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.IndicateurImpactEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.IndicateurImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.IndicateurImpactReseauDTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Mapper(componentModel = "spring")
public interface IndicateursMapper {

    ImpactEquipementPhysique toDomain(IndicateurImpactEquipementPhysiqueDTO dto);

    IndicateurImpactEquipementPhysiqueDTO toDTO(ImpactEquipementPhysique dto);

    ImpactEquipementVirtuel toDomain(IndicateurImpactEquipementVirtuelDTO dto);

    IndicateurImpactEquipementVirtuelDTO toDTO(ImpactEquipementVirtuel dto);

    IndicateurImpactApplicationDTO toDTO(ImpactApplication dto);

    @Mapping(target = "moisAnnee", source = "moisAnnee",qualifiedByName = "convertToLocalDate")
    IndicateurImpactMessagerieDTO toDTO(ImpactMessagerie dto);

    IndicateurImpactReseauDTO toIndicateur(ImpactReseau impactReseau);

    @Named("convertToLocalDate")
    default LocalDate convertToLocalDate(Integer moisAnnee){

        if (moisAnnee!=null){
            try {
                return LocalDate.parse(moisAnnee + "01", DateTimeFormatter.ofPattern("yyyyMMdd"));
            }catch (DateTimeParseException e){
                //
            }
        }
        return null;
    }
}
