package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.topic.data.ApplicationDTO;
import org.mte.numecoeval.topic.data.DataCenterDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.EquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;

@Mapper(componentModel = "spring")
public interface EntreesMapper {

    EquipementPhysique toDomain(EquipementPhysiqueDTO dto);

    DataCenter toDomain(DataCenterDTO dto);

    Application toDomain(ApplicationDTO dto);

    EquipementVirtuel toDomain(EquipementVirtuelDTO dto);

    Messagerie toDomain(MessagerieDTO messagerieDTO);

}
