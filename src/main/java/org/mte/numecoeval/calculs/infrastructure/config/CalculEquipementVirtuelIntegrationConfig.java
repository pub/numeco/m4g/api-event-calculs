package org.mte.numecoeval.calculs.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;
import org.mte.numecoeval.topic.data.MessageCalculEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactEquipementVirtuelDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.util.UUID;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.eqv"
)
public class CalculEquipementVirtuelIntegrationConfig {

    // Domain
    @Bean
    CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService(ObjectMapper objectMapper) {
        return new CalculImpactEquipementVirtuelServiceImpl(objectMapper);
    }

    // Propriétés
    @Value("${numecoeval.topic.calcul.equipementVirtuel}")
    String topicCalculEquipementVirtuel;

    @Value("${numecoeval.topic.indicateur.impactEquipementVirtuel}")
    String topicIndicateurImpactEquipementVirtuel;

    // Entrées
    @Bean
    public NewTopic topicCalculEquipementVirtuel() {
        return new NewTopic(topicCalculEquipementVirtuel, 1, (short) 1);
    }

    @Bean
    public MessageChannel calculImpactEquipementVirtuel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessageCalculEquipementVirtuelDTO> entreeCalculEquipementVirtuelKafkaMessageAdapter(KafkaMessageListenerContainer<String, MessageCalculEquipementVirtuelDTO> container) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(container, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("calculImpactEquipementVirtuel");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessageCalculEquipementVirtuelDTO> entreeCalculEquipementVirtuelKafkaContainer(ConsumerFactory<String, MessageCalculEquipementVirtuelDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicCalculEquipementVirtuel);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // CalculEquipementVirtuelIntegrationService

    // Sortie

    @Bean
    public NewTopic topicKafkaIndicateurImpactEquipementVirtuel() {
        return new NewTopic(topicIndicateurImpactEquipementVirtuel, 1, (short) 1);
    }

    @Bean
    public MessageChannel outputCalculImpactEquipementVirtuelToKafka() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "outputCalculImpactEquipementVirtuelToKafka")
    public MessageHandler indicateursImpactEquipementVirtuelHandler(KafkaTemplate<String, MessageIndicateurImpactEquipementVirtuelDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageIndicateurImpactEquipementVirtuelDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicIndicateurImpactEquipementVirtuel));
        return handler;
    }
}
