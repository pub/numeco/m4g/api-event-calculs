package org.mte.numecoeval.calculs.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicationService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactApplicationServiceImpl;
import org.mte.numecoeval.topic.data.MessageCalculIndicateurApplicationDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurApplicationDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.util.UUID;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.app"
)
public class CalculApplicationIntegrationConfig {

    // Domain
    @Bean
    CalculImpactApplicationService calculImpactApplicationService(ObjectMapper objectMapper) {
        return new CalculImpactApplicationServiceImpl(objectMapper);
    }

    // Propriétés
    @Value("${numecoeval.topic.calcul.application}")
    String topicCalculApplication;

    @Value("${numecoeval.topic.indicateur.application}")
    String topicIndicateurImpactApplication;

    // Entrées
    @Bean
    public NewTopic topicCalculApplication() {
        return new NewTopic(topicCalculApplication, 1, (short) 1);
    }

    @Bean
    public MessageChannel calculImpactApplication() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessageCalculIndicateurApplicationDTO> entreeCalculApplicationKafkaMessageAdapter(KafkaMessageListenerContainer<String, MessageCalculIndicateurApplicationDTO> container) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(container, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("calculImpactApplication");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessageCalculIndicateurApplicationDTO> entreeCalculApplicationKafkaContainer(ConsumerFactory<String, MessageCalculIndicateurApplicationDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicCalculApplication);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // CalculImpactApplicationService

    // Sortie

    @Bean
    public NewTopic topicKafkaIndicateurImpactApplication() {
        return new NewTopic(topicIndicateurImpactApplication, 1, (short) 1);
    }

    @Bean
    public MessageChannel outputCalculImpactApplicationToKafka() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "outputCalculImpactApplicationToKafka")
    public MessageHandler indicateursImpactApplicationHandler(KafkaTemplate<String, MessageIndicateurApplicationDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageIndicateurApplicationDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicIndicateurImpactApplication));
        return handler;
    }
}
