package org.mte.numecoeval.calculs.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactMessagerieServiceImpl;
import org.mte.numecoeval.topic.data.MessageCalculMessagerieDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactMessagerieDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.util.UUID;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.mes"
)
public class CalculMessagerieIntegrationConfig {

    // Domain
    @Bean
    CalculImpactMessagerieService calculImpactMessagerieService(ObjectMapper objectMapper) {
        return new CalculImpactMessagerieServiceImpl(objectMapper);
    }

    // Propriétés
    @Value("${numecoeval.topic.calcul.messagerie}")
    String topicCalculMessagerie;

    @Value("${numecoeval.topic.indicateur.messagerie}")
    String topicIndicateurImpactMessagerie;

    // Entrées
    @Bean
    public NewTopic topicCalculMessagerie() {
        return new NewTopic(topicCalculMessagerie, 1, (short) 1);
    }

    @Bean
    public MessageChannel calculImpactMessagerie() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessageCalculMessagerieDTO> entreeCalculMessagerieKafkaMessageAdapter(KafkaMessageListenerContainer<String, MessageCalculMessagerieDTO> container) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(container, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("calculImpactMessagerie");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessageCalculMessagerieDTO> entreeCalculMessagerieKafkaContainer(ConsumerFactory<String, MessageCalculMessagerieDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicCalculMessagerie);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // CalculImpactApplicationService

    // Sortie

    @Bean
    public NewTopic topicKafkaIndicateurImpactMessagerie() {
        return new NewTopic(topicIndicateurImpactMessagerie, 1, (short) 1);
    }

    @Bean
    public MessageChannel outputCalculImpactMessagerieToKafka() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "outputCalculImpactMessagerieToKafka")
    public MessageHandler indicateursImpactMessagerieHandler(KafkaTemplate<String, MessageIndicateurImpactMessagerieDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageIndicateurImpactMessagerieDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicIndicateurImpactMessagerie));
        return handler;
    }
}
