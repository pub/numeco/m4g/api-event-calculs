package org.mte.numecoeval.calculs.infrastructure.integration.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ExceptionHandler {


    @ServiceActivator(
            inputChannel = "errorChannel"
    )
    public void handleException(MessagingException messagingException) {
        if(messagingException != null
                && messagingException.getFailedMessage() != null) {
            log.error("Erreur gérable dans le traitement d'un message avec envoi sur topic de rejeu : message: {}", messagingException.getFailedMessage());

        }
        else {
            log.error("Erreur globale dans Spring Integration dans le traitement d'un message", messagingException);
        }

    }

}
