package org.mte.numecoeval.calculs.infrastructure.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementPhysiqueService;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.EtapeDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactEquipementPhysiqueDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Slf4j
@ConditionalOnProperty(
        value = "numecoeval.features.eqp"
)
@Service
@AllArgsConstructor
public class CalculEquipementPhysiqueIntegrationService {

    EntreesMapper entreesMapper;

    ReferentielMapper referentielMapper;

    IndicateursMapper indicateursMapper;

    CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService;

    @ServiceActivator(
            inputChannel = "calculImpactEquipementPhysique",
            outputChannel = "outputCalculImpactEquipementPhysiqueToKafka"
    )
    public MessageIndicateurImpactEquipementPhysiqueDTO calculImpactEquipementPhysique(CalculEquipementPhysiqueDTO calculEquipementPhysiqueDTO) {
        log.info("Calcul impact equipement physique : Nom d'équipement : {}, Nombre d'étape : {}, Nombre de Critère : {}",
                calculEquipementPhysiqueDTO.getEquipementPhysique().getNomEquipementPhysique(),
                CollectionUtils.size(calculEquipementPhysiqueDTO.getEtapes()),
                CollectionUtils.size(calculEquipementPhysiqueDTO.getCriteres())
        );
        LocalDateTime dateCalcul = LocalDateTime.now();
        MessageIndicateurImpactEquipementPhysiqueDTO messageIndicateurDTO = new MessageIndicateurImpactEquipementPhysiqueDTO();
        messageIndicateurDTO.setEquipementPhysique(calculEquipementPhysiqueDTO.getEquipementPhysique());
        messageIndicateurDTO.setImpactsEquipement(new ArrayList<>());
        for(EtapeDTO etapeDTO : CollectionUtils.emptyIfNull(calculEquipementPhysiqueDTO.getEtapes())) {
            for(CritereDTO critereDTO : calculEquipementPhysiqueDTO.getCriteres()) {
                DemandeCalculImpactEquipementPhysique demandeCalculImpactEquipementPhysique = DemandeCalculImpactEquipementPhysique.builder()
                        .dateCalcul(dateCalcul)
                        .equipementPhysique(entreesMapper.toDomain(calculEquipementPhysiqueDTO.getEquipementPhysique()))
                        .etape(referentielMapper.toEtape(etapeDTO))
                        .critere(referentielMapper.toCritere(critereDTO))
                        .typeEquipement(referentielMapper.toTypeEquipement(calculEquipementPhysiqueDTO.getTypeEquipement()))
                        .correspondanceRefEquipement(referentielMapper.toCorrespondanceRefEquipement(calculEquipementPhysiqueDTO.getCorrespondanceRefEquipement()))
                        .hypotheses(referentielMapper.toListHypothese(calculEquipementPhysiqueDTO.getHypotheses()))
                        .mixElectriques(referentielMapper.toListMixElectrique(calculEquipementPhysiqueDTO.getMixElectriques()))
                        .impactEquipements(referentielMapper.toListImpactEquipement(calculEquipementPhysiqueDTO.getImpactsEquipement()))
                        .build();
                var resultCalcul = calculImpactEquipementPhysiqueService.calculerImpactEquipementPhysique(demandeCalculImpactEquipementPhysique);
                messageIndicateurDTO.addImpactsEquipementItem(indicateursMapper.toDTO(resultCalcul));
            }
        }

        return messageIndicateurDTO;
    }
}
