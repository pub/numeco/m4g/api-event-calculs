package org.mte.numecoeval.calculs.infrastructure.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactReseau;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.EtapeDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactReseauDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Slf4j
@ConditionalOnProperty(
        value = "numecoeval.features.eqp"
)
@Service
@AllArgsConstructor
public class CalculImpactReseauIntegrationService {

    EntreesMapper entreesMapper;

    ReferentielMapper referentielMapper;

    IndicateursMapper indicateursMapper;

    CalculImpactReseauService calculImpactReseauService;

    @ServiceActivator(
            inputChannel = "calculImpactEquipementPhysique",
            outputChannel = "outputCalculImpactReseauToKafka"
    )
    public MessageIndicateurImpactReseauDTO calculImpactReseau(CalculEquipementPhysiqueDTO calculEquipementPhysiqueDTO) {
        log.info("Calcul impact réseau equipement physique : {}, Nombre d'étape : {}, Nombre de Critère : {}",
                calculEquipementPhysiqueDTO.getEquipementPhysique().getNomEquipementPhysique(),
                CollectionUtils.size(calculEquipementPhysiqueDTO.getEtapes()),
                CollectionUtils.size(calculEquipementPhysiqueDTO.getCriteres())
        );
        MessageIndicateurImpactReseauDTO messageIndicateursDTO = new MessageIndicateurImpactReseauDTO();
        messageIndicateursDTO.setImpactsReseaux(new ArrayList<>());
        LocalDateTime dateCalcul = LocalDateTime.now();
        for(EtapeDTO etape : CollectionUtils.emptyIfNull(calculEquipementPhysiqueDTO.getEtapes())) {
            for(CritereDTO critere : CollectionUtils.emptyIfNull(calculEquipementPhysiqueDTO.getCriteres())) {
                var demandeCalcul = DemandeCalculImpactReseau.builder()
                        .dateCalcul(dateCalcul)
                        .equipementPhysique(entreesMapper.toDomain(calculEquipementPhysiqueDTO.getEquipementPhysique()))
                        .etape(referentielMapper.toEtape(etape))
                        .critere(referentielMapper.toCritere(critere))
                        .impactsReseau(referentielMapper.toListImpactReseau(calculEquipementPhysiqueDTO.getImpactsReseau()))
                        .build();
                messageIndicateursDTO.addImpactsReseauxItem(indicateursMapper.toIndicateur(calculImpactReseauService.calculerImpactReseau(demandeCalcul)));
            }
        }

        return messageIndicateursDTO;
    }
}
