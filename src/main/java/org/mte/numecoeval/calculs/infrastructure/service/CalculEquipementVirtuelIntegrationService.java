package org.mte.numecoeval.calculs.infrastructure.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementVirtuelService;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapper;
import org.mte.numecoeval.topic.data.IndicateurImpactEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.MessageCalculEquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactEquipementVirtuelDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Slf4j
@ConditionalOnProperty(
        value = "numecoeval.features.eqv"
)
@Service
@AllArgsConstructor
public class CalculEquipementVirtuelIntegrationService {

    EntreesMapper entreesMapper;

    IndicateursMapper indicateursMapper;

    CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService;

    @ServiceActivator(
            inputChannel = "calculImpactEquipementVirtuel",
            outputChannel = "outputCalculImpactEquipementVirtuelToKafka"
    )
    public MessageIndicateurImpactEquipementVirtuelDTO calculImpactEquipementVirtuel(MessageCalculEquipementVirtuelDTO calculEquipementVirtuelDTO) {
        log.info("Calcul Impact équipement virtuel : Nom Equipement Virtuel : {}, Nom Equipement Physique : {}",
                calculEquipementVirtuelDTO.getEquipementVirtuel().getNomEquipementVirtuel(),
                calculEquipementVirtuelDTO.getEquipementVirtuel().getNomEquipementPhysique()
        );
        LocalDateTime dateCalcul = LocalDateTime.now();
        MessageIndicateurImpactEquipementVirtuelDTO messageIndicateurDTO = new MessageIndicateurImpactEquipementVirtuelDTO();
        messageIndicateurDTO.setEquipementVirtuel(calculEquipementVirtuelDTO.getEquipementVirtuel());
        messageIndicateurDTO.setImpactsEquipement(new ArrayList<>());
        for(IndicateurImpactEquipementPhysiqueDTO impactDTO : CollectionUtils.emptyIfNull(calculEquipementVirtuelDTO.getImpactsEquipement())) {
            var indicateur = calculImpactEquipementVirtuelService.calculerImpactEquipementVirtuel(
                    DemandeCalculImpactEquipementVirtuel.builder()
                            .dateCalcul(dateCalcul)
                            .equipementVirtuel(entreesMapper.toDomain(calculEquipementVirtuelDTO.getEquipementVirtuel()))
                            .nbEquipementsVirtuels(calculEquipementVirtuelDTO.getNbEquipementsVirtuels())
                            .nbTotalVCPU(calculEquipementVirtuelDTO.getNbTotalVCPU())
                            .stockageTotalVirtuel(calculEquipementVirtuelDTO.getStockageTotalVirtuel())
                            .impactEquipement(indicateursMapper.toDomain(impactDTO))
                            .build()
            );
            messageIndicateurDTO.addImpactsEquipementItem(indicateursMapper.toDTO(indicateur));
        }

        return messageIndicateurDTO;
    }
}
