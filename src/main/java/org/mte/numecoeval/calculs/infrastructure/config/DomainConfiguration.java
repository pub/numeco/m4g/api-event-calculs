package org.mte.numecoeval.calculs.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactReseauService;
import org.mte.numecoeval.calculs.domain.port.input.service.DureeDeVieEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactReseauServiceImpl;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.DureeDeVieEquipementPhysiqueServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfiguration {

    @Bean
    public DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService() {
        return new DureeDeVieEquipementPhysiqueServiceImpl();
    }

    @Bean
    public CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService(DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService, ObjectMapper objectMapper) {
        return new CalculImpactEquipementPhysiqueServiceImpl(
                dureeDeVieEquipementPhysiqueService,
                objectMapper
        );
    }

    @Bean
    public CalculImpactReseauService calculImpactReseauService(ObjectMapper objectMapper) {
        return new CalculImpactReseauServiceImpl(
                objectMapper
        );
    }
}
