package org.mte.numecoeval.calculs.infrastructure.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.IndicateursMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.MessageCalculMessagerieDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactMessagerieDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Slf4j
@ConditionalOnProperty(
        value = "numecoeval.features.mes"
)
@Service
@AllArgsConstructor
public class CalculMessagerieIntegrationService {

    EntreesMapper entreesMapper;

    ReferentielMapper referentielMapper;

    IndicateursMapper indicateursMapper;

    CalculImpactMessagerieService calculImpactMessagerieService;

    @ServiceActivator(
            inputChannel = "calculImpactMessagerie",
            outputChannel = "outputCalculImpactMessagerieToKafka"
    )
    public MessageIndicateurImpactMessagerieDTO calculImpactMessagerie(MessageCalculMessagerieDTO calculEquipementPhysiqueDTO) {
        LocalDateTime dateCalcul = LocalDateTime.now();
        MessageIndicateurImpactMessagerieDTO messageIndicateurDTO = new MessageIndicateurImpactMessagerieDTO();
        messageIndicateurDTO.setMessagerie(calculEquipementPhysiqueDTO.getMessagerie());
        messageIndicateurDTO.setIndicateurs(new ArrayList<>());
        for(CritereDTO critereDTO : CollectionUtils.emptyIfNull(calculEquipementPhysiqueDTO.getCriteres())) {
            log.info("Calcul impact messagerie : Critère: {}, Mois-Années: {}",
                    critereDTO.getNomCritere(),
                    calculEquipementPhysiqueDTO.getMessagerie().getMoisAnnee());
            DemandeCalculImpactMessagerie demandeCalcul = DemandeCalculImpactMessagerie.builder()
                    .dateCalcul(dateCalcul)
                    .messagerie(entreesMapper.toDomain(calculEquipementPhysiqueDTO.getMessagerie()))
                    .critere(referentielMapper.toCritere(critereDTO))
                    .impactsMessagerie(referentielMapper.toListImpactMessagerie(calculEquipementPhysiqueDTO.getImpactsMessagerie()))
                    .build();
            var resultCalcul = calculImpactMessagerieService.calculerImpactMessagerie(demandeCalcul);
            messageIndicateurDTO.addIndicateursItem(indicateursMapper.toDTO(resultCalcul));
        }

        return messageIndicateurDTO;
    }
}
