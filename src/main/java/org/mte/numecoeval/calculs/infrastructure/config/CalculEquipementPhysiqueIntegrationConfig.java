package org.mte.numecoeval.calculs.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.MessageIndicateurImpactReseauDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.util.UUID;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.eqp"
)
public class CalculEquipementPhysiqueIntegrationConfig {

    // Général

    @Value("${numecoeval.topic.indicateur.impactEquipementPhysique}")
    String topicIndicateurImpactEquipementPhysique;

    @Value("${numecoeval.topic.calcul.equipementPhysique}")
    String topicCalculEquipementPhysique;

    @Value("${numecoeval.topic.indicateur.impactReseau}")
    String topicImpactReseau;

    // Général Entrées

    @Bean
    public NewTopic topicCalculEquipementPhysique() {
        return new NewTopic(topicCalculEquipementPhysique, 1, (short) 1);
    }

    @Bean
    public MessageChannel calculImpactEquipementPhysique() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public KafkaMessageDrivenChannelAdapter<String, CalculEquipementPhysiqueDTO> entreeCalculEquipementPhysiqueKafkaMessageAdapter(KafkaMessageListenerContainer<String, CalculEquipementPhysiqueDTO> container) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(container, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("calculImpactEquipementPhysique");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, CalculEquipementPhysiqueDTO> entreeCalculEquipementPhysiqueKafkaContainer(ConsumerFactory<String, CalculEquipementPhysiqueDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(topicCalculEquipementPhysique);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // CalculImpactEquipementPhysique

    @Bean
    public NewTopic topicKafkaIndicateurImpactEquipementPhysique() {
        return new NewTopic(topicIndicateurImpactEquipementPhysique, 1, (short) 1);
    }

    @Bean
    public MessageChannel outputCalculImpactEquipementPhysiqueToKafka() {
        return new DirectChannel();
    }

    // Traitement voir classe CalculImpactEquipementPhysiqueService

    // Sortie
    @Bean
    @ServiceActivator(inputChannel = "outputCalculImpactEquipementPhysiqueToKafka")
    public MessageHandler indicateursImpactEquipementPhysiqueHandler(KafkaTemplate<String, MessageIndicateurImpactEquipementPhysiqueDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageIndicateurImpactEquipementPhysiqueDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicIndicateurImpactEquipementPhysique));
        return handler;
    }

    // CalculImpactReseau
    @Bean
    public NewTopic topicKafkaIndicateurImpactReseau() {
        return new NewTopic(topicImpactReseau, 1, (short) 1);
    }

    @Bean
    public MessageChannel outputCalculImpactReseauToKafka() {
        return new DirectChannel();
    }

    // Transformation

    // Traitement voir classe CalculImpactReseauService

    @Bean
    @ServiceActivator(inputChannel = "outputCalculImpactReseauToKafka")
    public MessageHandler indicateursImpactReseauHandler(KafkaTemplate<String, MessageIndicateurImpactReseauDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageIndicateurImpactReseauDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicImpactReseau));
        return handler;
    }
}
