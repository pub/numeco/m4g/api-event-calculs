package org.mte.numecoeval.calculs.infrastructure.config;

import org.mte.numecoeval.common.integration.interceptors.NumEcoEvalHeadersForLogChannelInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.GlobalChannelInterceptor;
import org.springframework.messaging.support.ChannelInterceptor;

@Configuration
public class CommonIntegrationConfig {

    @Bean
    @GlobalChannelInterceptor(patterns = "*")
    public ChannelInterceptor equipementPhysiqueChannelInterceptor() {
        return new NumEcoEvalHeadersForLogChannelInterceptor();
    }
}
