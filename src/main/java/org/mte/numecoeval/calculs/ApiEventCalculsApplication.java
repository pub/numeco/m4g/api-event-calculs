package org.mte.numecoeval.calculs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableIntegration
@EnableKafka
public class ApiEventCalculsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiEventCalculsApplication.class, args);
    }

}
